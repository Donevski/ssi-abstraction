# SSI Abstraction Service
## Description

<hr/>  
  <p align="center">A core service for the Organizational Credential Manager, providing the DIDComm functionality and initializing the agent, wallet and ledger interactions of the whole application.</p>

## Usage
<hr/>

### Endpoint documentation at: 

[Aries REST Extension](swagger.json)

[Full Agent Endpoints](AGENT-DOCUMENTATION.md)

[Full Agent Events](EVENTS-DOCUMENTATION.md)

[Sign and Verify Interface](SIGN-AND-VERIFY.md)

with the default exposed ports: 
* 3010 - Aries REST extension
* 3009 - full agent exposed
* 4000 - didcomm interface


## Installation
<hr/>

Dependencies:
```bash
$ yarn
```

* **If docker is not installed, [Install docker](https://docs.docker.com/engine/install/)**.
  

* **If docker-compose is not installed, [Install docker-compose](https://docs.docker.com/compose/install/)**.

* (optional) Postgres GUI 
https://dbeaver.io/download/

<hr/>


## Running the app
<hr/>

**Each service in the Organizational Credential Manager can be run from the infrastructure repository with Docker.**

**The .env files are in the infrastructure repository under /env**

### There are two separate Dockefiles in "./deployment" of every project:
```bash
    ## production in:
      ./deployment/ci
    ## development in:
      ./deployment/dev
```


* (optional) Edit docker-compose.yml in "infrastructure" to use either **/ci/** or **/dev/** Dockerfiles.

* Run while in **"infrastructure"** project:
```bash
$ docker-compose up --build
```


## Test
<hr/>

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```



## GDPR
<hr/>

[GDPR](GDPR.md)

## Dependencies
<hr/>

[Dependencies](package.json)

## License
<hr/>

[Apache 2.0 license](LICENSE)
