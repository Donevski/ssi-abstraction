# Agent Endpoints documentation

Documentation for all the allowed properties and methods of the aries agent object, accessed through an API.

## General form

```
/agent/:property/:method
```

with the following body

```
body: {
    subMethod: {
        name, //string
        subMethodData, //Array<any>
    }, 
    data, // Array<any>
}
```

where as subMethod.name, all the allowed properties and methods of what ":method" returns are passed as a string in object notation, e.g. 
```
{
    subMethod: {
        name: 'invitation.toUrl', // get the invitation as URL instead of object
        subMethodData: [
            {
                domain: 'http://localhost:4000/invitation'  // the invitation base URL
            }
        ]
    }
}
```

and data is an array of the arguments needed for this method.


<hr>

## Example 


Returns an invitation link:

### Endpoint: 
http://localhost:3009/agent/connections/createConnection

### body: 
```
{
    subMethod: {
        name: 'invitation.toUrl',
        subMethodData: [
            {
                domain: 'http://localhost:4000',
            }
        ],
    }
    data: [],
}
```


### Response
```
{
    statusCode: number,
    message: string,
    data: string, // invitation url
}
```

<hr>

## Allowed properties (/:property/)

<hr>


* connections
* proofs
* basicMessages
* ledger
* credentials
* mediationRecipient
* mediator
* discovery
* wallet

<hr>

## Allowed methods (/:method)

<hr>

## 1. For **connections** property

* createConnection
    * data:
    ```
    []
    ```

    #### subMethod.name
  1. 'invitation'
        * response:
        ```
        {
            statusCode,
            message,
            data,  //the invitation as a JSON
        }
        ```
  2. 'invitation.toUrl'
        * subMethodData:
        ```
        [
            domainBaseUrl: {
                domain: string;
            }, //base URL for the invitation link
        ]
        ```
        * response:
        ```
        {
            statusCode,
            message,
            data,  //the invitation URL
        }
        ```
  3. 'invitation.fromUrl'
        * subMethodData:
        ```
        [
            invitationUrl: string, //invitation url containing c_i or d_m parameter 
        ]
        ```
        * response:
        ```
        {
            statusCode,
            message,
            data,  //the invitation object
        }
        ```
    

## **!!! All methods bellow return the same object with the following subMethods:**
``` 
1.
    name: 'getTags'
    subMethodData: [];
```

```
2.
    name: 'myKey',
    subMethodData: [],
```

```
3. 
    name: 'theirKey',
    subMethodData: []
```

```
4.
    name: 'isReady'
    subMethodData: []
```

```
5. 
    name: 'assertReady',
    subMethodData: [],
```

```
6.
    name: 'assertState',
    subMethoData: [
        expectedStates: "invited" | "requested" | "responded" | "complete"
    ]
```

```
7. 
    name: 'assertRole'
    subMethoData: [
        expectedRole: "inviter" | "invitee"
    ],
```

* receiveInvitation
    * data:
    ```
    [
        invitationJson, //json object containing the invitation to receive
        config?: {
            autoAcceptConnection?: boolean;
            alias?: string;
            mediatorId?: string;
        }, //config for handling of invitation (optional)
    ]
    ```

* receiveInvitationFromUrl 
    * data:
    ```
    [
        invitationUrl: string, //url containing a base64 encoded invitation to receive
        config?: {
            autoAcceptConnection?: boolean;
            alias?: string;
            mediatorId?: string;
        }, //config for handling of invitation (optional)
    ]
    ```


* acceptInvitation
    * data:
    ```
    [
        connectionId: string, //the id of the connection for which to accept the invitation
    ]
    ```

* acceptRequest
    * data:
    ```
    [
        connectionId: string, //the id of the connection for which to accept the invitation
    ]
    ```

* acceptResponse
    * data:
    ```
    [
        connectionId: string, //the id of the connection for which to accept the invitation
    ]
    ```

* returnWhenIsConnected
    * data:
    ```
    [
        connectionId: string, //the id of the connection for which to accept the invitation
    ]
    ```

* getAll
    * data:
    ```
    []
    ```

* getById
    * data:
    ```
    [
        connectionId: string, //The connection record id
    ]
    ```
    * errors
        * @throws {RecordNotFoundError} If no record is found

* findById
    * data:
    ```
    [
        connectionId: string, //The connection record id
    ]
    ```

* deleteById
    * data:
    ```
    [
        connectionId: string, //The connection record id
    ]
    ```

* findByVerkey
    * data:
    ```
    [
        verkey: string, //The verkey to search for
    ]
    ```
    * errors
        * throws {RecordDuplicateError} if multiple connections are found for the given verkey

* findByTheirKey
    * data:
    ```
    [
        verkey: string, //The verkey to search for
    ]
    ```
    * errors
        * throws {RecordDuplicateError} if multiple connections are found for the given verkey

* findByInvitationKey
    * data:
    ```
    [
        key: string, //the invitation key to search for
    ]
    ```
    * errors
        * throws {RecordDuplicateError} if multiple connections are found for the given key

* getByThreadId
    * data:
    ```
    [
        threadId: string, //the thread id
    ]
    ```
    * errors
        * throws {RecordNotFoundError} If no record is found
        * throws {RecordDuplicateError} If multiple records are found



## 2. For **proofs** property




## **!!! All methods bellow return the same object with the following subMethods:**
``` 
1.
    name: 'getTags'
    subMethodData: [],
```

```
2.
    name: 'assertState',
    subMethoData: [
        expectedStates: "proposal-sent" | "proposal-received" | "request-sent" | "request-received" | "presentation-sent" | "presentation-received" | "declined" | "done"
    ]
```

```
3. 
    name: 'assertRole'
    subMethoData: [
        currentConnectionId: string,
    ],
```


* proposeProof
    * data:
    ```
    [
        connectionId: string, //The connection to send the proof proposal to
        presentationProposal, //The presentation proposal to include in the message
        config?: {
            comment?: string,
            autoAcceptProof?: AutoAcceptProof,
        }, //Additional configuration to use for the proposal
    ]
    ```

* acceptProposal
    * data:
    ```
    [
        proofRecordId: string, //The id of the proof record for which to accept the proposal
        config?: {
            request?: {
                name?: string;
                version?: string;
                nonce?: string;
            };
            comment?: string;
        } //Additional configuration to use for the request
    ]
    ```

* requestProof
    * data:
    ```
    [
        connectionId: string, //The connection to send the proof request to
        proofRequestOptions: {
            name,
            version,
            nonce,
            nonRevoked, //object containing "from, to" properties
            ver?: '1.0' | '2.0';
            requestedAttributes? ,
            requestedPredicates? ,
        }
        config?: {
            comment?: string,
            autoAcceptProof?: "always" | "contentApproved" | "never",
        }, //Options to build the proof request
    ]
    ```

* createOutOfBandRequest
    * data:
    ```
    [
        proofRequestOptions: {
            name,
            version,
            nonce,
            nonRevoked, //object containing "from, to" properties
            ver?: '1.0' | '2.0';
            requestedAttributes? ,
            requestedPredicates? ,
        }
        config?: {
            comment?: string,
            autoAcceptProof?: "always" | "contentApproved" | "never",
        }, //Options to build the proof request 
    ]
    ```

* acceptRequest
    * data:
    ```
    [
        proofRecordId: string, //The id of the proof record for which to accept the request
        requestedCredentials,  //The requested credentials object specifying which credentials to use for the proof
        config?: {
            comment?: string;
        } //Additional configuration to use for the presentation
    ]
    ```

* declineRequest
    * data:
    ```
    [
        proofRecordId: string, //the id of the proof request to be declined
    ]
    ```
        
* acceptPresentation
    * data:
    ```
    [
        proofRecordId: string, //The id of the proof record for which to accept the presentation
    ]
    ```

* getRequestedCredentialsForProofRequest
    * data:
    ```
    [
        proofRecordId: string, //the id of the proof request to get the matching credentials for
        config?: {
            filterByPresentationPreview?: boolean,
        } //optional configuration for credential selection process. Use `filterByPresentationPreview` (default `true`) to only include credentials that match the presentation preview from the presentation proposal (if available).
    ]
    ```

* autoSelectCredentialsForProofRequest
    * data:
    ```
    [
        retrievedCredentials, //The retrieved credentials object to get credentials from
    ]
    ```

* sendProblemReport
    * data:
    ```
    [
        proofRecordId: string, //The id of the proof record for which to send problem report
        message: string, //message to send
    ]
    ```

* getAll
    * data:
    ```
    []
    ```

* getById
    * data:
    ```
    [
        proofRecordId: string, //The proof record id
    ]
    ```
    * errors
        * throws {RecordNotFoundError} If no record is found
        * throws {RecordDuplicateError} If multiple records are found

* findById
    * data:
    ```
    [
        proofRecordId: string, //The proof record id
    ]
    ```

* deleteById
    * data:
    ```
    [
        proofRecordId: string, //The proof record id
    ]
    ```


## 3. For **basicMessages** property



* sendMessage
    * data:
    ```
    [
        connectionId: string, //the connection to which to send the message
        message: string, //the message to send
    ]
    ```

## **!!! The method bellow returns an object with the following subMethod:**
``` 
1.
    name: 'getTags'
    subMethodData: [],
```

* findAllByQuery
    * data:
    ```
    [
        query
    ]
    ```



## 4. For **ledger** property

* registerPublicDid
    * data: 
    ```
    [
        did: string, 
        verkey: string, 
        alias: string, 
        role?: 'TRUSTEE' | 'STEWARD' | 'TRUST_ANCHOR' | 'ENDORSER' | 'NETWORK_MONITOR';
    ]
    ```

* getPublicDid
    * data:
    ```
    [
        did: string,
    ]
    ```

* registerSchema
    * data:
    ```
    [
        schema: {
            name: string;
            version: string;
            attributes: string[];
        }
    ]
    ```

* getSchema
    * data:
    ```
    [
        id: string
    ]
    ```

* registerCredentialDefinition
    * data: 
    ```
    [
        credentialDefinitionTemplate: {
            schema: Schema;
            tag: string;
            supportRevocation: boolean;
        }
    ]
    ```

* getCredentialDefinition
    * data:
    ```
    [
        id: string
    ]
    ```

## 5. For **credentials** property

* deleteById
    * data:
    ```
    [
        credentialRecordId: string, //the credential record id
    ]
    ```


## **!!! All methods bellow return the same object with the following subMethods:**
``` 
1.
    name: 'getTags'
    subMethodData: [],
```

```
2.
    name: 'assertState',
    subMethoData: [
        expectedStates: "proposal-sent" | "proposal-received" | "offer-sent" | "offer-received" | "declined" | "request-sent" | "request-received" | "credential-issued" | "credential-received" | "done",
    ]
```

```
3. 
    name: 'getCredentialInfo'
    subMethoData: [],
```

```
4. 
    name: 'assertConnection'
    subMethoData: [
        currentConnectionId: string
    ],
```


* proposeCredential
    * data:
    ```
    [
        connectionId: string, //The connection to send the credential proposal to
        config?: {
            linkedAttachments?: Array<{
                attributeName: string,
                attachment: {
                    id: string,
                    /**
                    * An optional human-readable description of the content.
                    */
                    description?: string,
                    /**
                    * A hint about the name that might be used if this attachment is persisted as a file. It is not required, and need not be unique. If this field is present and mime-type is not, the extension on the filename may be used to infer a MIME type.
                    */
                    filename?: string,
                    /**
                    * Describes the MIME type of the attached content. Optional but recommended.
                    */
                    mimeType?: string,
                    /**
                    * A hint about when the content in this attachment was last modified.
                    */
                    lastmodTime?: Date,
                    /**
                    * Optional, and mostly relevant when content is included by reference instead of by value. Lets the receiver guess how expensive it will be, in time, bandwidth, and storage, to fully fetch the attachment.
                    */
                    byteCount?: number,
                    data: {
                        /**
                         * Base64-encoded data, when representing arbitrary content inline instead of via links. Optional.
                         */
                        base64?: string,
                        /**
                         *  Directly embedded JSON data, when representing content inline instead of via links, and when the content is natively conveyable as JSON. Optional.
                         */
                        json?,
                        /**
                         * A list of zero or more locations at which the content may be fetched. Optional.
                         */
                        links?: Array<string>,
                        /**
                         * A JSON Web Signature over the content of the attachment. Optional.
                         */
                        jws?,
                        /**
                         * The hash of the content. Optional.
                         */
                        sha256?: string,
                    }
                }
            }>
            autoAcceptCredential?: "always" | "contentApproved" | "never",
        } //Additional configuration to use for the proposal
    ]
    ```


* acceptProposal
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to accept the proposal
        config?: {
            comment?: string,
            credentialDefinitionId?: string,
            autoAcceptCredential?: "always" | "contentApproved" | "never",
        }
    ]
    ```


* negotiateProposal
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to accept the proposal
        preview, //The new preview for negotiation
        config?: {
            comment?: string,
            credentialDefinitionId?: string,
            autoAcceptCredential?: "always" | "contentApproved" | "never",
        }
    ]
    ```

* offerCredential
    * data:
    ```
    [
        connectionId: string, //The connection to send the credential offer to
        credentialTemplate: {
            credentialDefinitionId: string,
            comment?: string,
            preview,
            autoAcceptCredential?: "always" | "contentApproved" | "never",
            attachments?: Array<{
                attachment: {
                    id: string,
                    /**
                    * An optional human-readable description of the content.
                    */
                    description?: string,
                    /**
                    * A hint about the name that might be used if this attachment is persisted as a file. It is not required, and need not be unique. If this field is present and mime-type is not, the extension on the filename may be used to infer a MIME type.
                    */
                    filename?: string,
                    /**
                    * Describes the MIME type of the attached content. Optional but recommended.
                    */
                    mimeType?: string,
                    /**
                    * A hint about when the content in this attachment was last modified.
                    */
                    lastmodTime?: Date,
                    /**
                    * Optional, and mostly relevant when content is included by reference instead of by value. Lets the receiver guess how expensive it will be, in time, bandwidth, and storage, to fully fetch the attachment.
                    */
                    byteCount?: number,
                    data: {
                        /**
                         * Base64-encoded data, when representing arbitrary content inline instead of via links. Optional.
                         */
                        base64?: string,
                        /**
                         *  Directly embedded JSON data, when representing content inline instead of via links, and when the content is natively conveyable as JSON. Optional.
                         */
                        json?,
                        /**
                         * A list of zero or more locations at which the content may be fetched. Optional.
                         */
                        links?: Array<string>,
                        /**
                         * A JSON Web Signature over the content of the attachment. Optional.
                         */
                        jws?,
                        /**
                         * The hash of the content. Optional.
                         */
                        sha256?: string,
                    }
                }
            }>,
            linkedAttachments?: Array<{
                attributeName: string,
                attachment: {
                    id: string,
                    /**
                    * An optional human-readable description of the content.
                    */
                    description?: string,
                    /**
                    * A hint about the name that might be used if this attachment is persisted as a file. It is not required, and need not be unique. If this field is present and mime-type is not, the extension on the filename may be used to infer a MIME type.
                    */
                    filename?: string,
                    /**
                    * Describes the MIME type of the attached content. Optional but recommended.
                    */
                    mimeType?: string,
                    /**
                    * A hint about when the content in this attachment was last modified.
                    */
                    lastmodTime?: Date,
                    /**
                    * Optional, and mostly relevant when content is included by reference instead of by value. Lets the receiver guess how expensive it will be, in time, bandwidth, and storage, to fully fetch the attachment.
                    */
                    byteCount?: number,
                    data: {
                        /**
                         * Base64-encoded data, when representing arbitrary content inline instead of via links. Optional.
                         */
                        base64?: string,
                        /**
                         *  Directly embedded JSON data, when representing content inline instead of via links, and when the content is natively conveyable as JSON. Optional.
                         */
                        json?,
                        /**
                         * A list of zero or more locations at which the content may be fetched. Optional.
                         */
                        links?: Array<string>,
                        /**
                         * A JSON Web Signature over the content of the attachment. Optional.
                         */
                        jws?,
                        /**
                         * The hash of the content. Optional.
                         */
                        sha256?: string,
                    }
                }
            }>,
        }, //The credential template to use for the offer
    ]
    ```

* createOutOfBandOffer
    * data:
    ```
    [
        credentialTemplate: {
            credentialDefinitionId: string,
            comment?: string,
            preview,
            autoAcceptCredential?: "always" | "contentApproved" | "never",
            attachments?: Array<{
                attachment: {
                    id: string,
                    /**
                    * An optional human-readable description of the content.
                    */
                    description?: string,
                    /**
                    * A hint about the name that might be used if this attachment is persisted as a file. It is not required, and need not be unique. If this field is present and mime-type is not, the extension on the filename may be used to infer a MIME type.
                    */
                    filename?: string,
                    /**
                    * Describes the MIME type of the attached content. Optional but recommended.
                    */
                    mimeType?: string,
                    /**
                    * A hint about when the content in this attachment was last modified.
                    */
                    lastmodTime?: Date,
                    /**
                    * Optional, and mostly relevant when content is included by reference instead of by value. Lets the receiver guess how expensive it will be, in time, bandwidth, and storage, to fully fetch the attachment.
                    */
                    byteCount?: number,
                    data: {
                        /**
                         * Base64-encoded data, when representing arbitrary content inline instead of via links. Optional.
                         */
                        base64?: string,
                        /**
                         *  Directly embedded JSON data, when representing content inline instead of via links, and when the content is natively conveyable as JSON. Optional.
                         */
                        json?,
                        /**
                         * A list of zero or more locations at which the content may be fetched. Optional.
                         */
                        links?: Array<string>,
                        /**
                         * A JSON Web Signature over the content of the attachment. Optional.
                         */
                        jws?,
                        /**
                         * The hash of the content. Optional.
                         */
                        sha256?: string,
                    }
                }
            }>,
            linkedAttachments?: Array<{
                attributeName: string,
                attachment: {
                    id: string,
                    /**
                    * An optional human-readable description of the content.
                    */
                    description?: string,
                    /**
                    * A hint about the name that might be used if this attachment is persisted as a file. It is not required, and need not be unique. If this field is present and mime-type is not, the extension on the filename may be used to infer a MIME type.
                    */
                    filename?: string,
                    /**
                    * Describes the MIME type of the attached content. Optional but recommended.
                    */
                    mimeType?: string,
                    /**
                    * A hint about when the content in this attachment was last modified.
                    */
                    lastmodTime?: Date,
                    /**
                    * Optional, and mostly relevant when content is included by reference instead of by value. Lets the receiver guess how expensive it will be, in time, bandwidth, and storage, to fully fetch the attachment.
                    */
                    byteCount?: number,
                    data: {
                        /**
                         * Base64-encoded data, when representing arbitrary content inline instead of via links. Optional.
                         */
                        base64?: string,
                        /**
                         *  Directly embedded JSON data, when representing content inline instead of via links, and when the content is natively conveyable as JSON. Optional.
                         */
                        json?,
                        /**
                         * A list of zero or more locations at which the content may be fetched. Optional.
                         */
                        links?: Array<string>,
                        /**
                         * A JSON Web Signature over the content of the attachment. Optional.
                         */
                        jws?,
                        /**
                         * The hash of the content. Optional.
                         */
                        sha256?: string,
                    }
                }
            }>,
        }, //The credential template to use for the offer
    ]
    ```

* acceptOffer
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to accept the offer
        config?: {
            comment?: string,
            autoAcceptCredential?: "always" | "contentApproved" | "never",
        }
    ]
    ```

* declineOffer
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to decline the offer
    ]
    ```

* negotiateOffer
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to negotiate the offer
        preview,
        config?: {
            comment?: string;
            autoAcceptCredential?: "always" | "contentApproved" | "never",
        }
    ]
    ```

* acceptRequest
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to accept the request
        config?: {
            comment?: string;
            autoAcceptCredential?: "always" | "contentApproved" | "never",
        }
    ]
    ```

* acceptCredential
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to accept the credential
    ]
    ```

* sendProblemReport
    * data:
    ```
    [
        credentialRecordId: string, //The id of the credential record for which to send problem report
        message: string, //message to send
    ]
    ```

* getAll
    * data:
    ```
    []
    ```

* getById
    * data:
    ```
    [
        credentialRecordId: string, //the credential record id
    ]
    ```

* findById
    * data:
    ```
    [
        credentialRecordId: string, //the credential record id
    ]
    ```





## 6. For **mediatonRecipient** property

* initiateMessagePickup
    * data:
    ```
    [
        mediator
    ]
    ```

* pickupMessages
    * data:
    ```
    [
        mediatorConnection: Connection Record, //returned from connections property methods
    ]
    ```

* setDefaultMediator
    * data:
    ```
    [
        mediatorRecord: MediationRecord, //returned from mediatonRecipient methods
    ]
    ```

* notifyKeylistUpdate
    * data:
    ```
    [
        connection: ConnectionRecord, //returned from connections property methods
        verkey: string
    ]
    ```



## **!!! The method (findDefaultMediatorConnection) bellow return the same object with the following subMethods:**
``` 
1.
    name: 'getTags'
    subMethodData: [];
```

```
2.
    name: 'myKey',
    subMethodData: [],
```

```
3. 
    name: 'theirKey',
    subMethodData: []
```

```
4.
    name: 'isReady'
    subMethodData: []
```

```
5. 
    name: 'assertReady',
    subMethodData: [],
```

```
6.
    name: 'assertState',
    subMethoData: [
        expectedStates: "invited" | "requested" | "responded" | "complete"
    ]
```

```
7. 
    name: 'assertRole'
    subMethoData: [
        expectedRole: "inviter" | "invitee"
    ],
```



* findDefaultMediatorConnection
    * data:
    ```
    []
    ```



## **!!! All methods bellow return the same object with the following subMethods:**
``` 
1.
    name: 'getTags'
    subMethodData: [];
```

```
2.
    name: 'isReady'
    subMethodData: []
```

```
3. 
    name: 'assertReady',
    subMethodData: [],
```

```
4.
    name: 'assertState',
    subMethoData: [
        expectedStates: "requested" | "granted" | "denied",
    ],
```

```
5. 
    name: 'assertRole'
    subMethoData: [
        expectedRole: "MEDIATOR" | "RECIPIENT",
    ],
```



* discoverMediation
    * data:
    ```
    []
    ```



* requestMediation
    * data:
    ```
    [
        connection: Connection Record, //returned from connections property methods
    ]
    ```



* findByConnectionId
    * data:
    ```
    [
        connectionId: string,
    ]
    ```

* getMediators
    * data:
    ```
    []
    ```

* findDefaultMediator
    * data:
    ```
    [
        
    ]
    ```



* requestAndAwaitGrant
    * data:
    ```
    []
    ```

* provision
    * data:
    ```
    [
        mediatorConnInvite: string,
    ]
    ```


## 7. For **mediator** property

* queueMessage
    * data:
    ```
    [
        connectionId: string,
        message: {
            protected: unknown;
            iv: unknown;
            ciphertext: unknown;
            tag: unknown;
        }
    ]
    ```

## **!!! The method (grantRequestedMediation) bellow return the same object with the following subMethods:**
``` 
1.
    name: 'getTags'
    subMethodData: [];
```

```
2.
    name: 'isReady'
    subMethodData: []
```

```
3. 
    name: 'assertReady',
    subMethodData: [],
```

```
4.
    name: 'assertState',
    subMethoData: [
        expectedStates: "requested" | "granted" | "denied",
    ],
```

```
5. 
    name: 'assertRole'
    subMethoData: [
        expectedRole: "MEDIATOR" | "RECIPIENT",
    ],
```

* grantRequestedMediation
    * data:
    ```
    [
        mediatorId: string,
    ]
    ```




## 8. For **discovery** property

* queryFeatures
    * data: 
    ```
    [
        connectionId: string, 
        options: {
            query: string,
            comment?: string,
        }
    ]
    ```


## 9. For **wallet** property

* initialize
    * data:
    ```
    [
        walletConfig: {
            id: string;
            key: string;
        }
    ]
    ```

* create
    * data:
    ```
    [
        walletConfig: {
            id: string;
            key: string;
        }
    ]
    ```

* open
    * data:
    ```
    [
        walletConfig: {
            id: string;
            key: string;
        }
    ]
    ```

* close
    * data:
    ```
    []
    ```

* delete
    * data:
    ```
    []
    ```

* initPublicDid
    * data:
    ```
    [
        didConfig: {
            seed?: string;
        }
    ]
    ```

* createDid
    * data:
    ```
    [
        didConfig?: {
            seed?: string;
        } 
    ]
    ```

* pack
    * data:
    ```
    [
        payload,
        recipientKeys: Array<string>, 
        senderVerkey?: string,
    ]
    ```

* unpack
    * data:
    ```
    [
        encryptedMessage: {
            protected: unknown;
            iv: unknown;
            ciphertext: unknown;
            tag: unknown;
        }
    ]
    ```

* sign
    * data:
    ```
    [
        data: Buffer, 
        verkey: string,
    ]
    ```

* verify
    * data:
    ```
    [
        signerVerkey: string, 
        data: Buffer, 
        signature: Buffer,
    ]
    ```

* generateNonce
    * data:
    ```
    []
    ```

 