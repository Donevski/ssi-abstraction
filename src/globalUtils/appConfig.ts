import { VersioningType, INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';

/**
 * Microservice and versioning configuration of the service
 *
 * @param app - Nest.js internal configuration object
 * @param configService - Nest.js internal configuration object
 */
export default function appConf(
  app: INestApplication,
  configService: ConfigService,
): void {
  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.NATS,
    options: {
      servers: [configService.get('nats')?.url],
    },
  });

  app.enableVersioning({
    defaultVersion: ['1'],
    type: VersioningType.URI,
  });
}
