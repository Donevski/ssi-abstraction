import { Inject, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import logger from '@src/globalUtils/logger';
import {
  Agent,
  HttpOutboundTransport,
  InitConfig,
} from '@aries-framework/core';
import {
  LedgerIds,
  LedgerInfo,
  LEDGER_GENESIS,
  NYM_URL,
} from '@src/agent/agentUtils/ledgerConfig';
import { agentDependencies, HttpInboundTransport } from '@aries-framework/node';
import { subscribe } from '@src/agent/agentUtils/listener';
import { NatsClientService } from '@src/client/nats.client';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { NATSServices } from '@common/constants';
import config from '@config/config';
import axios from 'axios';

export const AGENT = 'agent';

const agentFactory = {
  provide: AGENT,
  useFactory: async (
    configService: ConfigService,
    natsClient: NatsClientService,
  ) => {
    logger.info('Agent initializing...');

    const {
      name,
      walletId,
      walletKey,
      ledgerIds,
      host,
      peerPort,
      path,
      publicDidSeed,
      autoAcceptConnection,
      autoAcceptCredential,
    } = configService.get('agent');

    const endpoints = [`${host}${peerPort}${path}`];

    const indyLedgers: LedgerInfo[] = ledgerIds.map((id: LedgerIds) => {
      const ledgerId: LedgerIds = id || 'BCOVRIN_TEST';
      const ledger: LedgerInfo = {
        id: `${ledgerId}_Genesis`,
        genesisTransactions: LEDGER_GENESIS?.[ledgerId],
        isProduction: false,
      };

      return ledger;
    });

    const agentConfig: InitConfig = {
      label: name,
      walletConfig: {
        id: walletId,
        key: walletKey,
      },
      indyLedgers,
      publicDidSeed,
      endpoints,
      autoAcceptConnections: autoAcceptConnection,
      autoAcceptCredentials: autoAcceptCredential,
    };

    const agent = new Agent(agentConfig, agentDependencies);

    const httpInbound = new HttpInboundTransport({
      port: Number(peerPort.replace(':', '')),
    });

    agent.registerInboundTransport(httpInbound);

    agent.registerOutboundTransport(new HttpOutboundTransport());

    await agent.initialize();
    await subscribe(agent, natsClient);

    if (agent.isInitialized) {
      ledgerIds.forEach((id: LedgerIds) => {
        axios
          .post(NYM_URL[id], {
            role: 'ENDORSER',
            alias: name,
            did: agent.publicDid?.did,
            verkey: agent.publicDid?.verkey,
          })
          .then((res: any) => {
            logger.info(JSON.stringify(res.data));
          })
          .catch((err: any) => {
            logger.error(err);
          });
      });
    }

    logger.info('Agent initialized');

    return agent;
  },
  inject: [ConfigService, NatsClientService],
};

@Module({
  imports: [
    ClientsModule.register([
      {
        name: NATSServices.SERVICE_NAME,
        transport: Transport.NATS,
        options: {
          servers: [config().nats.url as string],
        },
      },
    ]),
  ],
  controllers: [],
  providers: [agentFactory, NatsClientService],
  exports: [AGENT],
})
export class AgentModule {
  constructor(@Inject(AGENT) private agent: Agent) {}

  async onModuleDestroy() {
    await this.agent.shutdown();
  }
}

export default AgentModule;
